package praktikum7;

import java.util.Scanner;

/**
 * This file is belong to Universitas Kristen Immanuel
 *
 * @author Yoel Pieter S.
 */
public class HelloWorld {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Masukkan sebuah angka : ");
        int angka = sc.nextInt();
        if (angka % 3 == 0 && angka % 5 == 0) {
            System.out.println("Hello World");
        } else if (angka % 3 == 0) {
            System.out.println("Hello");
        } else if (angka % 5 == 0) {
            System.out.println("World");
        } else {
            System.out.println("Coba Lagi");
        }
    }
}
