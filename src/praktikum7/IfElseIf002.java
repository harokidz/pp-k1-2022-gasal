package praktikum7;

import java.util.Scanner;

/**
 * This file is belong to Universitas Kristen Immanuel
 *
 * @author Yoel Pieter S.
 */
public class IfElseIf002 {

    public static void main(String[] args) {
        //konversi nilai dari angka ke huruf
        //input
        Scanner sc = new Scanner(System.in);
        System.out.print("Masukkan Nilai anda : ");
        int nilaiAngka = sc.nextInt();
        //proses & output
        String output = "Anda mendapatkan nilai ";
        if (nilaiAngka > -1 && nilaiAngka < 21) {
            output += "E";
        } else if (nilaiAngka > 20 && nilaiAngka < 41) {
            output += "D";
        } else if (nilaiAngka > 40 && nilaiAngka < 61) {
            output += "C";
        } else if (nilaiAngka > 60 && nilaiAngka < 85) {
            output += "B";
        } else if (nilaiAngka > 84 && nilaiAngka < 101) {
            output += "A";
        } else {
            output = "Nilai yang anda masukkan salah 0..100";
        }
        System.out.println(output);

    }
}
