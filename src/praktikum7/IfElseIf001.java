package praktikum7;

import java.util.Scanner;

/**
 * This file is belong to Universitas Kristen Immanuel
 * @author Yoel Pieter S.
 */
public class IfElseIf001 {
    public static void main(String[] args) {
        //konversi nilai dari angka ke huruf
        //input
        Scanner sc = new Scanner(System.in);
        System.out.print("Masukkan Nilai anda : ");
        int nilaiAngka = sc.nextInt();
        //proses & output
        String output = "Anda mendapatkan nilai ";
        if(nilaiAngka >= 0 && nilaiAngka <= 20){
            output += "E";
        }else if(nilaiAngka >= 21 && nilaiAngka <= 40){
            output += "D";
        }else if(nilaiAngka >= 41 && nilaiAngka <= 60){
            output += "C";
        }else if(nilaiAngka >= 61 && nilaiAngka <= 84){
            output += "B";
        }else if(nilaiAngka >= 85 && nilaiAngka <= 100){
            output += "A";
        }else{
            output = "Nilai yang anda masukkan salah 0..100";
        }
        System.out.println(output);
        
    }

}
