package praktikum8;

/**
 * This file is belong to Universitas Kristen Immanuel
 *
 * @author Yoel Pieter S.
 */
public class ForD001 {

    public static void main(String[] args) {
        for (int i = 9; i >= 0; i--) {
            System.out.println(i);
        }
    }
}
