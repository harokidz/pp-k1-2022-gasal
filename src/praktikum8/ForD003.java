package praktikum8;

/**
 * This file is belong to Universitas Kristen Immanuel
 *
 * @author Yoel Pieter S.
 */
public class ForD003 {

    public static void main(String[] args) {
        int[] nilaiMatakuliah = {45, 32, 67, 32, 67};
        for (int i = nilaiMatakuliah.length - 1; i >= 0; i--) {
            int nilai = nilaiMatakuliah[i];
            System.out.println(nilai);
        }
    }
}
