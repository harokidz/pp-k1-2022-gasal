package praktikum9;

/**
 * This file is belong to Universitas Kristen Immanuel
 * @author Yoel Pieter S.
 */
public class While001 {
    public static void main(String[] args) {
        int i = 0;
        while(i < 100){
            System.out.println(i);
            i++;
        }
    }

}
