package praktikum9;

/**
 * This file is belong to Universitas Kristen Immanuel
 *
 * @author Yoel Pieter S.
 */
public class While003 {

    public static void main(String[] args) {
        int[] values = {45, 23, 56, 23, 54, 2345};
        int index = 0;
        while (index < values.length) {
            int value = values[index];
            System.out.println(value);
            index++;
        }

    }
}
