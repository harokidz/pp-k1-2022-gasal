package praktikum9;

import java.util.Scanner;

/**
 * This file is belong to Universitas Kristen Immanuel
 * @author Yoel Pieter S.
 */
public class While002 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Masukkan Nilai awal : ");
        int nilaiAwal = s.nextInt();
        System.out.print("Masukkan Nilai akhir : ");
        int nilaiAkhir = s.nextInt();
        while(nilaiAwal <= nilaiAkhir){
            System.out.println(nilaiAwal);
            nilaiAwal++;
        }
    }
}
