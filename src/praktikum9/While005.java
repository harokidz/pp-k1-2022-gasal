package praktikum9;

import java.util.Scanner;

/**
 * This file is belong to Universitas Kristen Immanuel
 *
 * @author Yoel Pieter S.
 */
public class While005 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        String ulang = "y";
        while (ulang.equalsIgnoreCase("y")) {
            //baagian proses aplikasi
            System.out.print("Masukkan nilai a = ");
            int a = s.nextInt();
            System.out.print("Masukkan nilai b = ");
            int b = s.nextInt();

            //tugas nya buat rumus
            double c = Math.sqrt((a * a) + (b * b));

            System.out.println("Nilai C adalah = " + c);

            //bagian untuk ulang
            System.out.print("Anda ingin ulangi aplikasi ini [y/n] : ");
            ulang = s.next();
        }
    }
}
