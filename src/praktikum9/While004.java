package praktikum9;

import java.util.Scanner;

/**
 * This file is belong to Universitas Kristen Immanuel
 * @author Yoel Pieter S.
 */
public class While004 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        String stop = "y";
        while(stop.equalsIgnoreCase("n")){
            System.out.print("Anda ingin keluar dari aplikasi [y/n] : ");
            stop = s.next();
        }
    }
}
