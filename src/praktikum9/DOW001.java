package praktikum9;

import java.util.Scanner;

/**
 * This file is belong to Universitas Kristen Immanuel
 *
 * @author Yoel Pieter S.
 */
public class DOW001 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        String stop = "y";
        do {
            System.out.print("Anda ingin keluar dari aplikasi [y/n] : ");
            stop = s.next();
        } while (stop.equalsIgnoreCase("n"));
    }
}
