package praktikum3;

import java.util.Scanner;

/**
 * This file is belong to Universitas Kristen Immanuel
 *
 * @author Yoel Pieter S.
 */
public class PitagorasA {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Masukkan nilai c = ");
        int c = scan.nextInt();
        System.out.print("Masukkan nilai b = ");
        int b = scan.nextInt();

        //tugas nya buat rumus
        double a = Math.sqrt((c * c) - (b * b));

        System.out.println("Nilai A adalah = " + a);

    }
}
