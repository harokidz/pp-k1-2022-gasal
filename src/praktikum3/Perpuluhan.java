package praktikum3;

import java.util.Scanner;

public class Perpuluhan {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Masukkan Gaji = ");
        int gaji = scan.nextInt();

        //hitung perpuluhan
        int perpuluhan = gaji / 10;
        int sisaGaji = gaji - perpuluhan;
        float sisaGaji2 = gaji * 0.9f;

        System.out.println("Perpuluhan = " + perpuluhan);
        System.out.println("Sisa Gaji  = " + sisaGaji);
        System.out.println("Sisa Gaji versi float  = " + sisaGaji2);
    }
}
