package praktikum3;

import java.util.Scanner;

/**
 * This file is belong to Universitas Kristen Immanuel
 *
 * @author Yoel Pieter S.
 */
public class PitagorasB {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Masukkan nilai c = ");
        int c = scan.nextInt();
        System.out.print("Masukkan nilai a = ");
        int a = scan.nextInt();

        //tugas nya buat rumus
        double b = Math.sqrt((c * c) - (a * a));

        System.out.println("Nilai B adalah = " + b);

    }
}
