package praktikum3;

import java.util.Scanner;

/**
 * This file is belong to Universitas Kristen Immanuel
 *
 * @author Yoel Pieter S.
 */
public class Pitagoras {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Masukkan nilai a = ");
        int a = scan.nextInt();
        System.out.print("Masukkan nilai b = ");
        int b = scan.nextInt();

        //tugas nya buat rumus
        double c = Math.sqrt((a * a) + (b * b));

        System.out.println("Nilai C adalah = " + c);

    }
}
