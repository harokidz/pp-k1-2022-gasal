package praktikum2;

/**
 * This file is belong to Universitas Kristen Immanuel
 *
 * @author Yoel Pieter S.
 */
public class LatihanIntLong {

    public static void main(String[] args) {
        int a = 100;
        int b = 3;
        int c = a * b;
        long d = (long) (a / b);
        float e = (float) a / (float) b;
        double f = (double) a / (double) b;
        System.out.println("Hasil kali = " + c);
        System.out.println("Hasil bagi bilangan bulat = " + d);
        System.out.println("Hasil bagi real = " + e);
        System.out.println("Hasil bagi real double = " + f);
    }
}
