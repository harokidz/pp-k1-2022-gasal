package praktikum2;

/**
 * This file is belong to Universitas Kristen Immanuel
 *
 * @author Yoel Pieter S.
 */
public class LatihanPersegiPanjang {

    public static void main(String[] args) {
        int panjang = 8;
        int lebar = 10;
        //hitunglah luas dan keliling persegi panjang
        int luas = panjang * lebar;
        int keliling = (2 * panjang) + (2 * lebar);
        System.out.println("Panjang = " + panjang);
        System.out.println("Lebar = " + lebar);
        System.out.println("Luas Persegi Panjang = " + luas);
        System.out.println("Keliling Persegi Panjang = " + keliling);
    }
}
