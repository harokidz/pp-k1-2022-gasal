package praktikum2;

/*
* Nim : 5464654
* Nama : Pieter Haro
*/
public class LatihanShort {
    public static void main(String[] args) {
        short a = -32768;
        short b = 100;
        short c = (short) (a - b);
        //Nilai minimal min -32768 dan Max 32767
        System.out.println("Hasil dari " + a + " - " + b + " = " + c);
    }
}
