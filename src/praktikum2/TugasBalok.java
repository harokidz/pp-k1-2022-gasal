package praktikum2;

import java.util.Scanner;

/**
 * This file is belong to Universitas Kristen Immanuel
 *
 * @author Yoel Pieter S.
 */
public class TugasBalok {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        //input
        System.out.print("Masukkan panjang balok = ");
        int panjang = scan.nextInt();
        System.out.print("Masukkan lebar balok = ");
        int lebar = scan.nextInt();
        System.out.print("Masukkan tinggi balok = ");
        int tinggi = scan.nextInt();
        
        //hitunglah luas dan keliling persegi panjang
        int volume = panjang * lebar * tinggi;
        int luasPermukaan = 2 * ((panjang * lebar) + (panjang * tinggi) + (lebar * tinggi));

//        System.out.println("Panjang = " + panjang);
//        System.out.println("Lebar = " + lebar);
//        System.out.println("Tinggi = " + tinggi);
        System.out.println("Luas Permukaan Balok = " + luasPermukaan);
        System.out.println("Volume Balok = " + volume);
    }
}
