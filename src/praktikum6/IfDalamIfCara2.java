package praktikum6;

import java.util.Scanner;

public class IfDalamIfCara2 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Masukkan sebuah bilangan lebih dari 100 = ");
        int bilangan = s.nextInt();
        String notif = "Harus lebih besar dari 100";
        if (bilangan > 100) {
            //cek ganjil genap
            notif = "Bilangan Ganjil";
            if (bilangan % 2 == 0) {
                notif = "Bilangan Genap";
            }
        }
        System.out.println(notif);
    }
}
