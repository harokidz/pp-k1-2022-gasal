package praktikum6;

import java.util.Scanner;

public class Soal3Cara1 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Nama Pelanggan = ");
        String nama = s.next();
        System.out.print("Total Belanja = ");
        float totalBelanja = s.nextFloat();
        
        if(totalBelanja > 275000){
            //kondisi jika benar
            float potongan = totalBelanja * 6.75f / 100f;
            float totalBayar = totalBelanja - potongan;
            System.out.println("Hi "+nama+", "
                    + "Anda mendapatkan potongan 6.75% sebesar Rp "+potongan+", "
                    + "Anda hanya perlu membayar Rp "+totalBayar);
        }else{
            //kondisi tidak memenuhi
            System.out.println("Harap Belanja lebih banyak "
                    + "agar anda mendapatkan potongan harga");
        }
    }
}
