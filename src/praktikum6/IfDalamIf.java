package praktikum6;

import java.util.Scanner;

public class IfDalamIf {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Masukkan sebuah bilangan lebih dari 100 = ");
        int bilangan = s.nextInt();
        if (bilangan > 100) {
            //cek ganjil genap
            if (bilangan % 2 == 0) {
                System.out.println("Bilangan Genap");
            } else {
                System.out.println("Bilangan Ganjil");
            }
        } else {
            System.out.println("Harus lebih besar dari 100");
        }
    }
}
