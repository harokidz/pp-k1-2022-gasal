package praktikum6;

import java.util.Scanner;

public class Soal3Cara2 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Nama Pelanggan = ");
        String nama = s.next();
        System.out.print("Total Belanja = ");
        float totalBelanja = s.nextFloat();

        String notif = "Harap Belanja lebih banyak "
                + "agar anda mendapatkan potongan harga";
        if (totalBelanja > 275000) {
            //kondisi jika benar
            float potongan = totalBelanja * 6.75f / 100f;
            float totalBayar = totalBelanja - potongan;
            notif = "Hi " + nama + ", "
                    + "Anda mendapatkan potongan 6.75% sebesar Rp " + potongan + ", "
                    + "Anda hanya perlu membayar Rp " + totalBayar;
        }
        System.out.println(notif);
    }
}
