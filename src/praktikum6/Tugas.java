package praktikum6;

import java.util.Scanner;

public class Tugas {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Masukkan sebuah bilangan lebih dari 100 = ");
        int bilangan = s.nextInt();
        String notif = "Harus lebih besar dari 100";
        if (bilangan >= 100) {
            //cek ganjil genap
            notif = "Bilangan Genap";
            if (bilangan % 2 != 0) {
                notif = "Bilangan Ganjil";
                if(bilangan % 9 == 0){
                    notif += "\nHello World";
                }
            }
        }
        System.out.println(notif);
    }
}
