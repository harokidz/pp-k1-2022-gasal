package teori;

import java.util.Scanner;

/**
 * This file is belong to Universitas Kristen Immanuel
 *
 * @author Yoel Pieter S.
 */
public class Test {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Masukkan Bilangan 1 : ");
        int b1 = s.nextInt();
        System.out.print("Masukkan Bilangan 2 : ");
        int b2 = s.nextInt();
        int i = 1;
        int hasilKali = 0;
        while (i <= b1) {
            hasilKali += b2;
            i++;
        }
        System.out.println("\nHasil kali = " + hasilKali);
    }
}
