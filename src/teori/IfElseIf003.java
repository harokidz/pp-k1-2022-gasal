package teori;

/**
 * This file is belong to Universitas Kristen Immanuel
 *
 * @author Yoel Pieter S.
 */
public class IfElseIf003 {

    public static void main(String[] args) {
        int nilai = 100;
        if (nilai < 0 || nilai > 100) {
            System.out.println("Salah");
        } else if (nilai >= 61) {
            System.out.println("A");
        } else if (nilai >= 85) {
            System.out.println("B");
        } else if (nilai >= 41) {
            System.out.println("C");
        } else if (nilai >= 21) {
            System.out.println("D");
        } else if (nilai >= 0) {
            System.out.println("E");
        }
    }

}
