package teori;

/**
 * This file is belong to Universitas Kristen Immanuel
 *
 * @author Yoel Pieter S.
 */
public class Switch {

    public static void coba(int bulanAngka) {
        System.out.println("Hallo selamat datang");
        System.out.println("Bulan yang anda masukkan adalah bulan " + bulanAngka);
    }

    public static void main(String[] args) {
        String bulanText = "Januari";
        int bulanAngka = 0;
        switch (bulanAngka) {
            case 0:
                bulanAngka = 1;
                coba(bulanAngka);
                break;
            case 1:
                bulanAngka = 2;
                coba(bulanAngka);
                break;
            case 3:
                bulanAngka = 3;
                coba(bulanAngka);
                break;
            default:
                bulanAngka = -1;
                break;
        }
    }
}
