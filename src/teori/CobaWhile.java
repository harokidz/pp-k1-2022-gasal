package teori;

/**
 * This file is belong to Universitas Kristen Immanuel
 * @author Yoel Pieter S.
 */
public class CobaWhile {
    public static void main(String[] args) {
        int a = 1;
        while(a < 10000){
            System.out.println(a++);
        }
    }
}
