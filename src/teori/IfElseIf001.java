package teori;

/**
 * This file is belong to Universitas Kristen Immanuel
 * @author Yoel Pieter S.
 */
public class IfElseIf001 {
    public static void main(String[] args) {
        int nilai = 65;
        if(nilai >= 85 && nilai <= 100){
            System.out.println("A");
        }else if(nilai >= 41 && nilai <= 60){
            System.out.println("C");
        }else if(nilai >= 61 && nilai <= 84){
            System.out.println("B");
        }else if(nilai >= 21 && nilai <= 40){
            System.out.println("D");
        }else if(nilai >=  0 && nilai <= 20){
            System.out.println("E");
        }else{
            //default
            System.out.println("Harus 0..100 saja");
        }
    }

}
