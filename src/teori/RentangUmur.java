package teori;

/**
 * This file is belong to Universitas Kristen Immanuel
 * @author Yoel Pieter S.
 */
public class RentangUmur {
    public static void main(String[] args) {
        int umur = 9;
        if(umur < 0){
            System.out.println("Salah");
        }else if(umur >= 51){
            System.out.println("Lansia");
        }else if(umur >= 25){
            System.out.println("Dewasa");
        }else if(umur >= 17){
            System.out.println("ABG");
        }else if(umur >= 11){
            System.out.println("Remaja");
        }else if(umur >= 4){
            System.out.println("Anak-anak");
        }else if(umur >= 0){
            System.out.println("Bayi");
        }
    }

}
