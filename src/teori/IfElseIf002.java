package teori;

/**
 * This file is belong to Universitas Kristen Immanuel
 * @author Yoel Pieter S.
 */
public class IfElseIf002 {
 public static void main(String[] args) {
        int nilai = 89;
        if(nilai >= 85 && nilai <= 100){
            System.out.println("A");
        }
        if(nilai >= 61 && nilai <= 84){
            System.out.println("B");
        }
        if(nilai >= 41 && nilai <= 60){
            System.out.println("C");
        }
        if(nilai >= 21 && nilai <= 40){
            System.out.println("D");
        }
        if(nilai >=  0 && nilai <= 20){
            System.out.println("E");
        }
    }
}
