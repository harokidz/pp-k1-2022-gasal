package teori;

import java.util.Scanner;

/**
 * This file is belong to Universitas Kristen Immanuel
 *
 * @author Yoel Pieter S.
 */
public class Test2 {
    private static int pangkat(int bilangan, int pangkat){
        int i = 1;
        int hasilKali = 1;
        while (i <= pangkat) {
            hasilKali *= bilangan;
            i++;
        }
        return hasilKali;
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
//        System.out.print("Masukkan Bilangan 1 : ");
//        int b1 = s.nextInt();
//        System.out.print("Masukkan Pangkat : ");
//        int bPangkat = s.nextInt();
        System.out.println("\nHasil Pangkat = " + pangkat(2,5));
        System.out.println("\nHasil Pangkat = " + pangkat(2,6));
        System.out.println("\nHasil Pangkat = " + pangkat(3,5));
        System.out.println("\nHasil Pangkat = " + pangkat(8,5));
    }
}
