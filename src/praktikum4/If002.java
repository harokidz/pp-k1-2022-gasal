package praktikum4;

/**
 * This file is belong to Universitas Kristen Immanuel
 * @author Yoel Pieter S.
 */
public class If002 {
    public static void main(String[] args) {
        int umur = 17;
        if(umur >= 18){
            //hanya dieksekusi jika kondisi benar
            System.out.println("Anda sudah cukup umur");
        }else{
            //hanya dieksekusi jika semua kondisi tidak terpenuhi
            System.out.println("Anda belum cukup umur");
        }
    }
}
