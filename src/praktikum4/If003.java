package praktikum4;

/**
 * This file is belong to Universitas Kristen Immanuel
 *
 * @author Yoel Pieter S.
 */
public class If003 {

    public static void main(String[] args) {
        int umur = 17;
        String jenisKelamin = "l";
        float tinggi = 169.99f;

        //syarat masuk polisi laki-laki :
        //1. umur > 16 dan dibawah 22
        //2. jenisKelamin laki-laki
        //3. tinggi minimal 170
        if (umur > 16 && jenisKelamin.equalsIgnoreCase("L") && tinggi > 170f) {
            System.out.println("Anda Memenuhi syarat menjadi polisi laki-laki");
        } else {
            System.out.println("Anda Tidak Memenuhi syarat menjadi polisi laki-laki");
        }
    }
}
